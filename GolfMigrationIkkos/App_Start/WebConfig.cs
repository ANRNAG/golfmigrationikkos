﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace GolfMigrationIkkos
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            config.EnableCors();

            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );


            // Remove the XML formatter
            config.Formatters.Remove(config.Formatters.XmlFormatter);

            // Add the JSON formatter
            config.Formatters.Add(config.Formatters.JsonFormatter);
            // config.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
        }
    }
}