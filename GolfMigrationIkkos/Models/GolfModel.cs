﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace GolfMigrationIkkos.Models
{
    public static class connectionclass
    {
        //internal static SqlConnection ConnectTOIkkosDB()
        //{
        //    string connectionString = ConfigurationManager.ConnectionStrings["CopyMe"].ConnectionString;
        //    SqlConnection sqlconn = new SqlConnection(connectionString);
        //    sqlconn.Open();
        //    return sqlconn;
        //}
      internal static SqlConnection ConnectTODB()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["Golf"].ConnectionString;
            SqlConnection sqlconn = new SqlConnection(connectionString);
            sqlconn.Open();
            return sqlconn;
        }
    }
    public partial class UserInfoDTO
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int Gender { get; set; }
        public string ProfilePicUrl { get; set; }
        public string Password { get; set; }
        public DateTime CreatedDate { get; set; }

        public string updatestatus { get; set; }

        internal static List<UserInfoDTO> GetAllUserDetails()
        {
            List<UserInfoDTO> userinfoList = new List<UserInfoDTO>();

            try
            {
                SqlConnection myConn = connectionclass.ConnectTODB();

                if (null != myConn)
                {
                      SqlCommand cmd = new SqlCommand("select * from [dbo].[User]  where email is not null", myConn);
                   // SqlCommand cmd = new SqlCommand("select * from [dbo].[User]  where email is not null and id > 6251 and id < 6280", myConn);
                    cmd.CommandType = CommandType.Text;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        UserInfoDTO userinfo = new UserInfoDTO();
                        //ID FirstName	LastName  Email	ProfilePicURL Password	PhoneNumber	PhoneID	OrganizationID	Address1	Address2	City	State	Zip	Country	Age	Gender	Type	Status	CreatedBy	CreateDate	UpdatedBy	UpdateDate	swim_exp	working_with_coach	subcategoryid	level	month	year	FacebookId	

                        userinfo.ID = (int)myData["ID"];
                        userinfo.FirstName = Convert.ToString(myData["FirstName"]);
                        userinfo.LastName = Convert.ToString(myData["LastName"]);
                        userinfo.Email = Convert.ToString(myData["Email"]);
                        userinfo.ProfilePicUrl = Convert.ToString(myData["ProfilePicURL"]);
                        userinfo.Gender = Convert.ToInt32(myData["Gender"]);
                        userinfo.Password = Convert.ToString(myData["Password"]);
                        userinfoList.Add(userinfo);
                    }
                    myData.Close();
                    myConn.Close();
                }
                return userinfoList;
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }
        internal static List<UserInfoDTO> GetAllUsersRange(int startid, int endid)
        {
            List<UserInfoDTO> userinfoList = new List<UserInfoDTO>();

            try
            {
                SqlConnection myConn = connectionclass.ConnectTODB();

                if (null != myConn)
                { // select* from[dbo].[User] where email is not null and id >= 1 and Id <= 13
                    SqlCommand cmd = new SqlCommand("select * from [dbo].[User]  where email is not null and id >= "+startid+" and Id <= "+endid, myConn);
                    // SqlCommand cmd = new SqlCommand("select * from [dbo].[User]  where email is not null and id > 6251 and id < 6280", myConn);
                    cmd.CommandType = CommandType.Text;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        UserInfoDTO userinfo = new UserInfoDTO();
                        //ID FirstName	LastName  Email	ProfilePicURL Password	PhoneNumber	PhoneID	OrganizationID	Address1	Address2	City	State	Zip	Country	Age	Gender	Type	Status	CreatedBy	CreateDate	UpdatedBy	UpdateDate	swim_exp	working_with_coach	subcategoryid	level	month	year	FacebookId	

                        userinfo.ID = (int)myData["ID"];
                        userinfo.FirstName = Convert.ToString(myData["FirstName"]);
                        userinfo.LastName = Convert.ToString(myData["LastName"]);
                        userinfo.Email = Convert.ToString(myData["Email"]);
                        userinfo.ProfilePicUrl = Convert.ToString(myData["ProfilePicURL"]);
                        userinfo.Gender = Convert.ToInt32(myData["Gender"]);
                        userinfo.Password = Convert.ToString(myData["Password"]);
                        userinfo.CreatedDate = Convert.ToDateTime(myData["CreateDate"]);
                        userinfoList.Add(userinfo);
                    }
                    myData.Close();
                    myConn.Close();
                }
                return userinfoList;
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

    }
    public partial class GolfUserInfoDTO : UserInfoDTO
    {
        public string UserName { get; set; }
        public string PhoneNumber { get; set; }
        public int PhoneID { get; set; }
        public int OrganizationID { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string UserSports { get; set; }
        public int Age { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public int ShardID { get; set; }
        public int ExpYearsInSwimming { get; set; }
        public Boolean WorkingWithCoach { get; set; }
        public int SubCategoryId { get; set; }
        public string Level { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public string FacebookId { get; set; }
        public string Organization { get; set; }   //by lohit
    }
    public partial class UserVideoInfoDTO
    {
        public int Id { get; set; }
        public string VideoURL { get; set; }
        public string ActorName { get; set; }
        public string MovementName { get; set; }
        public string IntroVideoURL { get; set; }
        public int Status { get; set; }
        public int CreatedBy { get; set; }
        public int Duration { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string ThumbNailURL { get; set; }
        public int VideoMode { get; set; }
        public int SportId { get; set; }
        public int LoginType { get; set; }
        public string Format { get; set; }
        public string updatestatus { get; set; }

        //internal static List<UserVideoInfoDTO> GetAllUserVideos()
        //{
        //    List<UserVideoInfoDTO> VideoInfoList = new List<UserVideoInfoDTO>();

        //    try
        //    {
        //        SqlConnection myConn = connectionclass.ConnectTODB(); 

        //        if (null != myConn)
        //        {
        //            SqlCommand cmd = new SqlCommand("select * from [dbo].[RecordedVideo] ", myConn);
        //            cmd.CommandType = CommandType.Text;

        //            SqlDataReader myData = cmd.ExecuteReader();

        //            while (myData.Read())
        //            {
        //                UserVideoInfoDTO uservideoinfo = new UserVideoInfoDTO();
        //                /* ID MovementName ActorName	Format VideoURL ThumbNailURL VideoRate
        //                 * Status CreatedBy CreateDate UpdatedBy	UpdateDate	Duration
        //                 * 
        //                 * ID  MovementName  ActorName Format Duration VideoURL ThumbNailURL
        //                 * Status CreatedBy CreateDate UpdatedBy UpdateDate
        //                */

        //                uservideoinfo.Id = (int)myData["ID"];
        //                uservideoinfo.MovementName = Convert.ToString(myData["MovementName"]);
        //                uservideoinfo.ActorName = Convert.ToString(myData["ActorName"]);
        //                uservideoinfo.Format = Convert.ToString(myData["Format"]);
        //                uservideoinfo.Duration = Convert.ToInt32(myData["Duration"]);
        //                uservideoinfo.VideoURL = Convert.ToString(myData["VideoURL"]);
        //                uservideoinfo.ThumbNailURL = Convert.ToString(myData["ThumbNailURL"]);
        //                uservideoinfo.Status = Convert.ToInt32(myData["Status"]);
        //                uservideoinfo.CreatedBy = Convert.ToInt32(myData["CreatedBy"]);

        //              //  uservideoinfo.SportId = Convert.ToInt32(myData[""]);
        //                VideoInfoList.Add(uservideoinfo);
        //            }
        //            myData.Close();
        //            myConn.Close();
        //        }
        //        return VideoInfoList;
        //    }
        //    catch (Exception e)
        //    {
        //        string s = e.Message;
        //        throw;
        //    }
        //}
        internal static List<UserVideoInfoDTO> GetAllUserVideosCreatedBy(int userid)
        {
            List<UserVideoInfoDTO> VideoInfoList = new List<UserVideoInfoDTO>();

            try
            {
                SqlConnection myConn = connectionclass.ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("select * from [dbo].[RecordedVideo] where createdby = " +userid, myConn);
                    cmd.CommandType = CommandType.Text;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        UserVideoInfoDTO uservideoinfo = new UserVideoInfoDTO();
                        /* ID MovementName ActorName	Format VideoURL ThumbNailURL VideoRate
                         * Status CreatedBy CreateDate UpdatedBy	UpdateDate	Duration
                         * 
                         * ID  MovementName  ActorName Format Duration VideoURL ThumbNailURL
                         * Status CreatedBy CreateDate UpdatedBy UpdateDate
                        */

                        uservideoinfo.Id = (int)myData["ID"];
                        uservideoinfo.MovementName = Convert.ToString(myData["MovementName"]);
                        uservideoinfo.ActorName = Convert.ToString(myData["ActorName"]);
                        uservideoinfo.Format = Convert.ToString(myData["Format"]);
                        uservideoinfo.Duration = Convert.ToInt32(myData["Duration"]);
                        uservideoinfo.VideoURL = Convert.ToString(myData["VideoURL"]);
                        uservideoinfo.ThumbNailURL = Convert.ToString(myData["ThumbNailURL"]);
                        uservideoinfo.Status = Convert.ToInt32(myData["Status"]);
                        uservideoinfo.CreatedDate = Convert.ToDateTime(myData["CreateDate"]);
                        uservideoinfo.CreatedBy = Convert.ToInt32(myData["CreatedBy"]);

                        //  uservideoinfo.SportId = Convert.ToInt32(myData[""]);
                        VideoInfoList.Add(uservideoinfo);
                    }
                    myData.Close();
                    myConn.Close();
                }
                return VideoInfoList;
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }
       
    }


    //public partial class VideoInfoDTO
    //{
    //    //  [CreateUserLibraryVideo]
    //    //        @VideoURL varchar(1024),	@MomentName varchar(50),	@CreatedBy int,	@ThumbNailURL varchar(1024),	@Format varchar(50),
    //    // @VideoMode int, 	@Id int, 	@SportId int, 	@LoginType int,	@UserId int

    //    public int Id { get; set; }
    //    public string VideoUrl { get; set; }
    //    public string MomentName { get; set; }
    //    public int CreatedBy { get; set; }
    //    public string ThumbnailUrl { get; set; }
    //    public string Format { get; set; }
    //    public int VideoMode { get; set; }
       
    //    public int SportId { get; set; }
    //    public int LoginType { get; set; }
    //    public int UserId { get; set; }

    //    internal static int UploadVideoToDB(VideoInfoDTO videos)
    //    {
    //        int result = 0;

    //        try
    //        {
    //            SqlConnection myConn = connectionclass.ConnectTOIkkosDB();
    //            //    MultiShardConnection multiShardConnection = BaseUtils.getMultiShardConnection();
    //            //       MultiShardCommand sqlCommand = multiShardConnection.CreateCommand();
    //            SqlCommand sqlCommand = new SqlCommand();

    //            sqlCommand.CommandText = "[CreateUserLibraryVideo]";
    //            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
    //            sqlCommand.Parameters.AddWithValue("@UserId", videos.UserId);
    //            sqlCommand.Parameters.AddWithValue("@ThumbNailUrl", videos.ThumbnailUrl);
    //            sqlCommand.Parameters.AddWithValue("@VideoUrl", videos.VideoUrl);
    //            sqlCommand.Parameters.AddWithValue("@MomentName", videos.MomentName);
    //            sqlCommand.Parameters.AddWithValue("@CreatedBy", videos.CreatedBy);
    //            sqlCommand.Parameters.AddWithValue("@Format", videos.Format);
    //            sqlCommand.Parameters.AddWithValue("@SportId", videos.SportId);
    //            sqlCommand.Parameters.AddWithValue("@VideoMode", videos.VideoMode);
    //            sqlCommand.Parameters.AddWithValue("@Id", videos.Id);
    //            sqlCommand.Parameters.AddWithValue("@LoginType", videos.LoginType);

    //            SqlDataReader reader = sqlCommand.ExecuteReader(); 

    //            while (reader.Read())
    //            {
    //                result = reader["Result"] == DBNull.Value ? 0 : Convert.ToInt32(reader["Result"]);
    //            }
    //        }
    //        catch (Exception e)
    //        {
    //            var Message = e.Message;
                
    //            throw;
    //        }
    //        return result;
    //    }
    //}
}

