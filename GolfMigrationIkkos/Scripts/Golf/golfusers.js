﻿

uservalmodule.controller("golfusers", ['webAPIurl','IkkosWebApiurl',  '$scope', '$http', '$interval', function (webAPIurl, IkkosWebApiurl, $scope, $http, $interval) {

    var waituserinsert = false;
    var waitvideoinsert = false;
    var waitgetGulfUserVideos = false;
    var waitgetIkkosUserIdVideos = false;
    var ikkosid = 0;

    var usercount = 0;
    var videocount = 0;
    var insertuserInterval;
    var insertVideoInterval;

    $scope.Breakupdateuserandvideos = function () {
        $scope.Userlist[usercount].updatestatus = 'updation stoped' ;
        
        videocount = usercount = 100000000;
       // waituserinsert = true;
      //  waitvideoinsert = true;
        $scope.displaystatus = 'stopped by user..';
        $interval.cancel(insertuserInterval);
    }

    //$scope.displaystatus = 'Enter Users range from Last Id and select get users';

    $scope.getuserslist = function () {
        $scope.displaystatus = 'Getting Golf User details pls wait..';
        var getusers = $http({
            method: "get",
            url: webAPIurl + '/api/GolfAPI/GetAllUsersRange/?startid=' + $scope.userfromID + '&endid=' + $scope.userendID,
            dataType: 'json',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json, text/javascript"
            }
        }).then(function (res, status) {
            //       alert('userlookup  ' + JSON.stringify(res.data.Response));
            $scope.Userlist = res.data.Response;
            $scope.displaystatus = '';
            console.log($scope.Userlist);
            $scope.waitdisplay = true;
        }, function error(responce) {
            console.log(responce.data.Message);
            $scope.displaystatus = responce.data.Message;
        });
    }

    var getGulfuservideofunction = function (userid) {
        waitgetGulfUserVideos = false;
        $scope.displaystatus = 'Getting Golf User Videos pls wait..';
        var getuservideos = $http({
            method: "get",
            url: webAPIurl + '/api/GolfAPI/GetAllUservideos/?userid='+userid,
            dataType: 'json',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json, text/javascript"
            }
        }).then(function (res, status) {
            //       alert('userlookup  ' + JSON.stringify(res.data.Response));
            $scope.GulfUserVideolist = res.data.Response;
            $scope.displaystatus = '';
            console.log($scope.GulfUserVideolist);
            $scope.videowaitdisplay = true;
            waitgetGulfUserVideos = true;
        }, function error(responce) {
            console.log(responce.data.Message);
            $scope.displaystatus = responce.data.Message;
            waitgetGulfUserVideos = true;
        });
    };

    var getIkkosUserIdVideofunction = function (gulfuseremail) {
        waitgetIkkosUserIdVideos = false;
        $scope.displaystatus = 'Getting Ikkos User Videos pls wait..';
        var getuservideos = $http({
            method: "get",
            url: IkkosWebApiurl + 'VideosV3/GetLibraryVideosByUserEmail/?useremail='+gulfuseremail,
            dataType: 'json',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json, text/javascript"
            }
        }).then(function (res, status) {
            var IkkosVideolist = res.data.Response;
            ikkosid = IkkosVideolist[0].UserId;
           console.log('ikkos video data ' + IkkosVideolist);
           compareGolfIkkosVideo(IkkosVideolist);
           $scope.displaystatus = '';
            waitgetIkkosUserIdVideos = true;
        }, function error(responce) {
            console.log(responce.data.Message);
            $scope.displaystatus = responce.data.Message;
            waitgetIkkosUserIdVideos = true;
        });
    };
   
    $scope.updateuserandvideos = function () {
        
        var value = '';
        usercount = 0;
        videocount = 0;
        waituserinsert = false;
        waitvideoinsert = false;
        waitgetGulfUserVideos = false;
        waitgetIkkosUserIdVideos = false;

        insertuserInterval = $interval(function () {

                if (waituserinsert == false && usercount < $scope.Userlist.length)
                {
                    value = $scope.Userlist[usercount];
                    var UserInfoDto = {
                        "UserId": value.ID,
                        "Name": value.FirstName + ' ' + value.LastName,
                        "email": value.Email,
                        "ProfilePicUrl": value.ProfilePicUrl,
                        "FacebookId": null,
                        "Gender": value.Gender,
                        "LoginType": 2,
                        "DeviceId": 0,
                        "DeviceType": 0,
                        "createddate": '',
                        "CreatedBy": '',
                        "SportId": 7,
                        "Password": value.Password
                    }

                    waituserinsert = true; 
                    insertIkkosUserfunction(UserInfoDto);
                }
            // user insertion completed 
                if (usercount >= $scope.Userlist.length)
                {
                    $interval.cancel(insertuserInterval);
                    $scope.displaystatus = 'all records updated successffully';
                }
                   
            }, 500);
    }

    var insertIkkosUserfunction = function (UserInfoDto) {

        // getting perticular user videos
       getGulfuservideofunction(UserInfoDto.UserId);

        $scope.Userlist[usercount].updatestatus = 'Updating... ';

        var insertIkkosuser = $http({
            method: 'post',
            url: IkkosWebApiurl+'UsersV2/CreateUserV2',
            dataType: 'json',
            data: UserInfoDto,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json, text/javascript"
            }
        }).then(function (res, status) {
            //       alert('userlookup  ' + JSON.stringify(res.data.Response));
            var IkkosUser = res.data.Response;
      //      console.log(IkkosUser);
             if (res.data.Status == 0)
                {
                    $scope.Userlist[usercount].updatestatus = IkkosUser.UserId;

                    var waitGulfUserVideos = $interval(function () {

                        if (waitgetGulfUserVideos)
                        {
                            if ($scope.GulfUserVideolist.length > 0) {
                                $scope.displaystatus = IkkosUser.UserId + ' User videos updating....  ';
                                updateIkkosVideoFunction(IkkosUser.UserId);
                            }
                            else {
                                waituserinsert = false;
                                usercount = usercount + 1
                            }
                            $interval.cancel(waitGulfUserVideos);
                            waitgetGulfUserVideos = false;
                        }
                    }, 500)
                   
                }
                // wait for get user videos from getGulfuservideofunction
            // User already exist
             else if (res.data.Status == 1) {

                 var waitGulfUserExistVideos = $interval(function () {

                     if (waitgetGulfUserVideos)
                     {
                         waitgetGulfUserVideos = false;
                         $interval.cancel(waitGulfUserExistVideos);

                         if ($scope.GulfUserVideolist.length > 0) {
                             waitgetIkkosUserIdVideos = false;
                             getIkkosUserIdVideofunction(UserInfoDto.email)

                             var waitikkisvideo = $interval(function () {
                                 if (waitgetIkkosUserIdVideos) {
                                     //$scope.displaystatus = 'all records updated successffully';
                                     $scope.displaystatus = ikkosid + ' User videos updating....  ';
                                     updateIkkosVideoFunction(ikkosid);
                                     //      $scope.Userlist[usercount].updatestatus = 'write code to update with exist videos'
                                     waitgetIkkosUserIdVideos = false;
                                   //  waituserinsert = false;
                                     $interval.cancel(waitikkisvideo)
                                 //    usercount = usercount + 1;
                                 }
                             }, 500)
                         }
                         else {
                             waituserinsert = false;
                             $scope.Userlist[usercount].updatestatus = res.data.Message + '  No videos to update'
                             usercount = usercount + 1;
                         }
                     }
                   
                 }, 500)

            }
            else
            {
                $scope.Userlist[usercount].updatestatus = res.data.Message;
                waituserinsert = false;
                usercount = usercount + 1;
            }
                
            

        }, function error(responce) {
            waituserinsert = false;
            console.log(responce.data.Message);
            $scope.Userlist[usercount].updatestatus = responce.data.Message;
            $scope.displaystatus = responce.data.Message;
            usercount = usercount + 1;
        });

    }

    var updateIkkosVideoFunction = function (IkkosUserId) {
        var tempusercount = usercount;

        var videostatusdisplay = $scope.Userlist[usercount].updatestatus + ' Updating videos: '  + $scope.GulfUserVideolist.length + '  of ';
        $scope.Userlist[usercount].updatestatus = videostatusdisplay;

        insertVideoInterval = $interval(function () {
          

            if (waitvideoinsert == false && videocount < $scope.GulfUserVideolist.length) {

                $scope.Userlist[usercount].updatestatus = videostatusdisplay + (videocount + 1);

               var videovalue = $scope.GulfUserVideolist[videocount];
                var VideoInfoDto = {
                    "Id":0,
                    "VideoUrl": videovalue.VideoURL, 
                    "MomentName": videovalue.MovementName,
                    "CreatedBy": 0,
                    "ThumbnailUrl": videovalue.ThumbNailURL,
                    "Format": videovalue.Format,
                    "VideoMode": videovalue.VideoMode,
                    "SportId": 7,
                    "LoginType": videovalue.LoginType,
                    "UserId": IkkosUserId
                }

                
                if (videovalue.updatestatus == null)
                {
                    waitvideoinsert = true;
                    inserIkkosVideofunction(VideoInfoDto);
                }
                    
                else
                {
                    waitvideoinsert = false;
                    videocount = videocount + 1;
                //    usercount = usercount + 1;
                }
                    

            }
            // user insertion completed 
            if (videocount >= $scope.GulfUserVideolist.length) {
                //if (tempusercount == usercount)
                //    usercount += 1;
                $scope.Videostopint();
                $scope.displaystatus = 'all Videos updated successffully';
            }

        }, 500);

        $scope.Videostopint = function () {
            $interval.cancel(insertVideoInterval);
            // updating user wait to continue next record
            waituserinsert = false;
            videocount = 0;
            usercount = usercount + 1;
        }

    }

    var inserIkkosVideofunction = function (VideoInfoDto) {

        $scope.GulfUserVideolist[videocount].updatestatus = 'Updating... ';
       
        var insertIkkosvideo = $http({
            method: 'post',
            url: IkkosWebApiurl+'VideosV3/UploadVideoToDB',
            dataType: 'json',
            data: VideoInfoDto,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json, text/javascript"
            }
        }).then(function (res, status) {
            //       alert('userlookup  ' + JSON.stringify(res.data.Response));
            var VideoId = res.data.Response;
            //      console.log(IkkosUser);
            if (res.data.Status == 0) 
                $scope.GulfUserVideolist[videocount].updatestatus = 'Successfully updated';
            else 
                $scope.GulfUserVideolist[videocount].updatestatus = res.data.Message;
            
            waitvideoinsert = false;
            videocount = videocount + 1;

        }, function error(responce) {
            console.log(responce.data.Message);
            $scope.GulfUserVideolist[videocount].updatestatus = responce.data.Message;
            $scope.displaystatus = responce.data.Message;

            waitvideoinsert = false;
            videocount = videocount + 1;

        });


    }

    var compareGolfIkkosVideo = function (IkkosVideo) {

        angular.forEach(IkkosVideo, function (IkkosValue, Ikkoskey) {
            angular.forEach($scope.GulfUserVideolist, function (Golfvalue, Golfkey) {
                if(angular.equals(IkkosValue.Libraryvideourl, Golfvalue.VideoURL)  )
                {
                    $scope.GulfUserVideolist[Golfkey].updatestatus = 'video already exist';
                }
            });

        //    if (value.Password == "thomasTheKing")
   //             console.log("username is thomas");
        });


        // $scope.GulfUserVideolist
    }


}]);