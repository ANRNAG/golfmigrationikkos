﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GolfMigrationIkkos.Models;
using System.Web.Http.Cors;

namespace GolfMigrationIkkos.WebApi
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class GolfAPIController : ApiController
    {
        private Utility.CustomResponse _Result = new Utility.CustomResponse();
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
    //    [Route("GetGolfUsers")]

        public Utility.CustomResponse GetAllUsers()
        {
            try
            {
                _Result.Response = UserInfoDTO.GetAllUserDetails();
                _Result.Status = Utility.CustomResponseStatus.Successful;
                _Result.Message = "successfull";
                return _Result;
            }
            catch (Exception ex)
            {
                _Result.Response = null;
                _Result.Status = Utility.CustomResponseStatus.Exception;
                _Result.Message = ex.Message;
                return _Result;
            }
            
        }
        public Utility.CustomResponse GetAllUsersRange(int startid, int endid)
        {
            try
            {
                _Result.Response = UserInfoDTO.GetAllUsersRange(startid, endid);
                _Result.Status = Utility.CustomResponseStatus.Successful;
                _Result.Message = "successfull";
                return _Result;
            }
            catch (Exception ex)
            {
                _Result.Response = null;
                _Result.Status = Utility.CustomResponseStatus.Exception;
                _Result.Message = ex.Message;
                return _Result;
            }

        }

        //public Utility.CustomResponse GetAllUserVideos()
        //{
        //    try
        //    {
        //        _Result.Response = UserVideoInfoDTO.GetAllUserVideos();
        //        _Result.Status = Utility.CustomResponseStatus.Successful;
        //        _Result.Message = "successfull";
        //        return _Result;
        //    }
        //    catch (Exception ex)
        //    {
        //        _Result.Response = null;
        //        _Result.Status = Utility.CustomResponseStatus.Exception;
        //        _Result.Message = ex.Message;
        //        return _Result;
        //    }

        //}

        public Utility.CustomResponse GetAllUserVideos(int userid)
        {
            try
            {
                _Result.Response = UserVideoInfoDTO.GetAllUserVideosCreatedBy(userid);
                _Result.Status = Utility.CustomResponseStatus.Successful;
                _Result.Message = "successfull";
                return _Result;
            }
            catch (Exception ex)
            {
                _Result.Response = null;
                _Result.Status = Utility.CustomResponseStatus.Exception;
                _Result.Message = ex.Message;
                return _Result;
            }

        }

        //[HttpPost]
        //public Utility.CustomResponse InsertUserVideos(VideoInfoDTO UserVideo)
        //{
        //    try
        //    {
        //        _Result.Response = VideoInfoDTO.UploadVideoToDB(UserVideo);
        //        _Result.Status = Utility.CustomResponseStatus.Successful;
        //        _Result.Message = "successfull";
        //        return _Result;
        //    }
        //    catch (Exception ex)
        //    {
        //        _Result.Response = null;
        //        _Result.Status = Utility.CustomResponseStatus.Exception;
        //        _Result.Message = ex.Message;
        //        return _Result;
        //    }

        //}
        // POST api/<controller>
     
    }
}