﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GolfMigrationIkkos
{
    public class Utility
    {
        public class CustomResponse
        {
            public CustomResponseStatus Status;
            public Object Response;
            public string Message;
        }

        public enum CustomResponseStatus
        {
            Successful,
            UnSuccessful,
            Exception
        }
    }

    public static class CustomConstants
    {
        public static string User_Does_Not_Exist = "User does not exist";
        public static string User_Exist_With_Email = "User already exist";
        public static string InValid_Credential = "InValid credential";
        public static string Registered_Successfully = "Registered successfully";
        public static string Payment_IsDone_Successfully = "Payment is processed successfully";
        public static string Video_Added_Successfully = "Video added succcessfully";
        public static string Video_Uploaded_Successfully = "Success - your video will be online in 24 hours!";
        public static string Video_Updated_Successfully = "Video updated succcessfully";
        public static string Access_Denied = "Access denied";
        public static string Video_DoesNot_Exists = "Video does not exist";
        public static string Video_Deleted_Successfully = "Video deleted successfully";
        public static string Video_rating_Added_Successfully = "Video rating succcessfully added";
        public static string PaymentHistory_Details_Get_Successfully = "Data retrieved successfully";
        public static string UpdateUser_Update_Successfully = "User updated successfully";
        public static string Visibility_Update_Successfully = "Video visibility updated successfully";
        public static string Visibility_Update_Fail = "Failed to update video visibility";
        public static string Video_Flag_Added_Successfully = "Video flagReason succcessfully added";
        public static string Password_MisMatch = "Password mismatched";
        public static string Usage_Tracking_Successfully = "Usage tracking inserted successfully";
        public static string PushNotifications = "Push notifications sent successfully";
        public static string NoRecordsFound = "No records found";
        public static string Details_Get_Successfully = "Data retrieved successfully";
        public static string Video_Accepted_Successfully = "Video accepted successfully";
        public static string Video_Rejected_Successfully = "Video rejected";
        public static string Uploaded_Successfully = "Uploaded successfully";
        public static string Content_Added_Successfully = "Content added successfully";
        public static string Content_Updated_Successfully = "Content updated successfully";
        public static string Content_Deleted_Successfully = "Content deleted successfully";
        public static string Personalization_Added_Successfully = "Personalization details added successfully";
        public static string Personalization_Updated_Successfully = "Personalization details updated successfully";
        public static string UserTrainingPlan_Added_Successfully = "User training plan details added successfully";
        public static string UserTrainingPlan_Updated_Successfully = "User training plan details updated successfully";
        public static string TrainingPlan_Added_Successfully = "Training plan details added successfully";
        public static string TrainingPlan_Updated_Successfully = "Training plan details updated successfully";
        public static string TrainingVideo_Added_Successfully = "Training video details added successfully";
        public static string TrainingVideo_Updated_Successfully = "Training video details updated successfully";
        public static string TrainingPlan_StartDate_Updated_Successfully = "Training plan startdate updated successfully";
        public static string RecordedVideo_Added_Successfully = "Recorded video added successfully";
        public static string Exception_Added_Successfully = "Exception added successfully";
        public static string TrainingPlan_Exists = "Plan already exists";
        public static string Log_In_Successful = "Login successful";
        public static string MailSent = "Mail sent successfully";
        public static string Email_Not_sent = "Error occured while sending mail";
        public static string ResetPassword = "Password has been reset successfully";
        public static string ResetPassword_Failed = "Resetting password has been failed.Try again!";
        public static string PromotionStatus_Updated_Successfully = "Status has been updated successfully";
        public static string ForgotPassword_successfully = "Mail sent successfully to registered email address";
        public static string Payment_added_Successfully = "Payment added successfully";
        public static string Login_Failed = "Please login to check the videos";
        public static string CopyRightIssue = "Cannot use this video as it contains copyrighted material";
        public static string videoNotExist = "Format incorrect. Try the YouTube web link instead of the YouTube app link.";
        public static string ThumbnailUpdateSuccess = "THumbnail Updated Successfully!!!";
        public static string ThumbnailUpdateFail = "Thumbnail Updation Failed!!!";


    }
}
